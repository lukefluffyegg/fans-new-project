<?php

use App\Authentication\JwtAuth;
use App\Http\Middleware\Authenticate;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/auth/login', 'Authentication\LoginController@index');
Route::post('/auth/register', 'Authentication\RegisterController@index');
Route::get('/me', 'User\UserController@index')->middleware(Authenticate::class);
