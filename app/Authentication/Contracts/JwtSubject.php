<?php 

namespace App\Authentication\Contracts;

interface JwtSubject {
	public function getJwtIdentifier();
}