<?php

namespace App\Authentication\Providers\Jwt;

interface JwtProviderInterface {
    public function encode(array $claims);
    public function decode($token);
}