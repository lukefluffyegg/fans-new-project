<?php 

namespace App\Authentication\Providers\Authentication;

interface AuthServiceProvider {
    public function byCredentials($email, $password);
    public function byId($id);
}